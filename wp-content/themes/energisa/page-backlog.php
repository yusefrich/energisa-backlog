<?php get_header(); /* Template Name: Backlog */ ?>
<section id="product-banner">
    <div style="
            background: linear-gradient(0deg, rgba(8, 155, 192, 0.7), rgba(8, 155, 192, 0.7)), url(<?php bloginfo('template_url'); ?>/img/novidades-header.png);
            background-size: cover;
            background-position: center;
            background-blend-mode: multiply, normal;
            " class="container-fluid   product-banner-holder px-0 ">
        <!-- text-white -->
        <div class="product-banner text-center text-white ">
            <h2 data-aos="fade-right">Ola mundo</h2>
            <p>Ola mundo</p>
        </div>
    </div>
</section>


<?php include "footer-nav.php"; ?>

<?php get_footer(); ?>

